package com.gitlab.nuf.gangspotions.core;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Created by nuf on 5/30/2016.
 */
public final class GangsPotions extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
    }

    @EventHandler
    public void onUseItem(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_AIR) {
            if (event.getPlayer().getItemInHand() != null) {
                if (event.getPlayer().getItemInHand().getType().equals(Material.BLAZE_POWDER) && event.getPlayer().getGameMode() != GameMode.CREATIVE) {
                    PotionEffect potionEffect = new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 1000, 1);
                    event.getPlayer().addPotionEffect(potionEffect);
                    event.getPlayer().getItemInHand().setAmount(event.getPlayer().getItemInHand().getAmount() - 1);
                }
            }
        }
    }
}
